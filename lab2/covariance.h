#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <type_traits> // for std::is_same

#include <omp.h>
#include <mpi.h>


// ===================================== serial version =======================================

template <typename T>
T mean_serial(T* vector, const int N)
{
    T mean = 0;
    for (int i=0; i<N; i++)
    {
        mean += vector[i];
    }
    return mean/N;
}


template <typename T>
T covariance_serial(T* vector1, T* vector2, const int N)
{
    T Cij = 0;
    T mean1 = mean_serial(vector1, N);
    T mean2 = mean_serial(vector2, N);
    
    for (int i=0; i<N; i++)
    {
        Cij += vector1[i]*vector2[i];
    }
    
    return Cij/N - mean1*mean2;
}


// ======================================= omp version ========================================


template <typename T>
  T mean_omp(T* vector, int nthreads, const int N)
{
    T mean = 0;
    
    // TODO: What happens with #pragma omp simd
    #pragma omp parallel for reduction(+:mean) num_threads(nthreads)
    for (int i=0; i<N; i++)
    {
        mean += vector[i];
    }
    return mean/N;
}


template <typename T>
T covariance_omp(T* vector1, T* vector2, int nthreads, const int N)
{
    T Cij = 0;
    T mean1 = mean_omp<T>(vector1, nthreads, N);
    T mean2 = mean_omp<T>(vector2, nthreads, N);
    
    #pragma omp parallel for reduction(+:Cij) num_threads(nthreads)
    for (int i=0; i<N; i++)
    {
        Cij += vector1[i]*vector2[i];
    }
    
    return Cij/N - mean1*mean2;
}



// ======================================= mpi version ========================================


template <typename T>
T mean_mpi(T* vector, int nthreads, const int N, MPI_Comm mpi_comm = MPI_COMM_WORLD)
{
    int ip, np;
    MPI_Comm_rank (mpi_comm, &ip);
    MPI_Comm_size (mpi_comm, &np);
    
    // evaluate mean value
    T mean = mean_omp<T>(vector, nthreads, N);
    T global_mean = 0;
    
    // here perform reduction
    if      ( std::is_same<T,float>::value )  MPI_Allreduce(&mean,&global_mean,1, MPI_FLOAT,  MPI_SUM, mpi_comm);
    else if ( std::is_same<T,double>::value ) MPI_Allreduce(&mean,&global_mean,1, MPI_DOUBLE, MPI_SUM, mpi_comm);
    
    return global_mean/np;
}


template <typename T>
T covariance_mpi(T* vector1, T* vector2, int nthreads, const int N, MPI_Comm mpi_comm = MPI_COMM_WORLD)
{
    int ip, np;
    MPI_Comm_rank (mpi_comm, &ip);
    MPI_Comm_size (mpi_comm, &np);
    
    T Cij = covariance_omp<T>(vector1, vector2, nthreads, N);
    T covariance = 0;
    
    // here perform reduction
    if      ( std::is_same<T,float>::value )  MPI_Allreduce(&Cij,&covariance,1, MPI_FLOAT,  MPI_SUM, mpi_comm);
    else if ( std::is_same<T,double>::value ) MPI_Allreduce(&Cij,&covariance,1, MPI_DOUBLE, MPI_SUM, mpi_comm);
    
    
    return covariance/np;
}



