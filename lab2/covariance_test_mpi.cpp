#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include <omp.h>
#include <mpi.h>


// user defined includes
#include "covariance.h"


//#include <random>
#include <unistd.h>
#include <time.h>
//#include "MPIEnvironment.hpp"


#define ACCURACY 0e-10
    
int main(int argc, char **argv) {
        
    MPI_Init(&argc, &argv);
    const int N = 1<<28;

    int ip, np;
    MPI_Comm_rank (MPI_COMM_WORLD, &ip);
    MPI_Comm_size (MPI_COMM_WORLD, &np);
    const int N_local = N/np;
    int nthreads = atoi(argv[2]);
    printf("ip=%d \t chunk size: %d/%d\n",ip,N_local,N);
    printf("Threads: %d\n", nthreads);
    printf("Processes: %d\n", np);
    size_t size = N_local;
    if (ip == 0) size=N;
    double* vec1 = (double*) malloc( size*sizeof(double) );
    double* vec2 = (double*) malloc( size*sizeof(double) );

    MPI_Barrier( MPI_COMM_WORLD );
    if (ip == 0) printf("Memory allocation ended\n");
    sleep(1);
    MPI_Barrier( MPI_COMM_WORLD );

    srand( time(NULL) );

    for (int i=0; i<N_local; i++)
    {
      vec1[i] = (1.0 - 2.0*rand()/((double) RAND_MAX));
      vec2[i] = (1.0 - 2.0*rand()/((double) RAND_MAX));
    }
    // force OpenMP to use threads                                                                                                            
    omp_set_dynamic(0);       // Explicitly disable dynamic teams                                                                             
    omp_set_num_threads(nthreads); // Use 4 threads for all consecutive parallel regions                                                             

    MPI_Barrier( MPI_COMM_WORLD );

    double t1, t2, timeDiff;

    t1 = MPI_Wtime();
    // compute covariance - test case and refrence                                                                                            
    double cov12_mpi    =    covariance_mpi<double>(vec1, vec2, nthreads, N_local);
    t2 = MPI_Wtime();
    timeDiff = t2-t1;

    MPI_Barrier( MPI_COMM_WORLD );

    // collect all vectors in root process (we arbitraly choose ip=0)                                                                         
    MPI_Gather(vec1, N_local, MPI_DOUBLE, vec1, N_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(vec2, N_local, MPI_DOUBLE, vec2, N_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (ip == 0) /* only single process */
      {
        double cov12_serial = covariance_serial<double>(vec1, vec2, N);
        printf("\n");
        printf("cov12_serial: %lf\n", cov12_serial);
        printf("time diff:    %lf\n", timeDiff);
	printf("\n");

	// Save result to file                                                 
	std::ofstream outfile;
	outfile.open("data.csv", std::ios_base::app);
	outfile << np << ", " << nthreads << ", " << timeDiff << std::endl;
      }

    free(vec1);
    free(vec2);
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}

