# Laboratorium 3 - wstęp do CUDA

## CUDA

CUDA to zestaw narzędzi programistycznych ułatwiających programowanie kart graficznych.
Zawiera zestaw bibliotek, sterowników oraz odpowiednie kompilatory i profilery.

[https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)

Ważne pojęcia:
- host - to samo co węzeł obliczeniowy
- device - karta graficzna, przykład zewnętrznego akceleratora obliczeń (czyli pamięć karty i RAM węzła obliczeniowego są rozdzielone)
- kernel - funkcja wywoływana po stronie hosta, ale wykonywana na karcie graficznej. 

By umożliwić masowe zrównoleglanie operacji karta graficzna ma specyficzny model pamięci. Jego zrozumienie jest niezbędne do poprawnego projektowania aplikacji na karcie graficznej.
Proszę zwrócić uwagę na rozdział: [https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#memory-hierarchy](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#memory-hierarchy).

Należy przeanalizować wybrany przykład kodu CUDA i odpowiedzieć czym jest
 * wątek
 * warp
 * blok
 * sieć bloków (grid)
 * stream

Jakie kroki należy wykonać by uruchomić kernel?

Przykładowe programy można znaleźć w [https://github.com/zchee/cuda-sample](https://github.com/zchee/cuda-sample) lub w katalogu lab3 w repozytorium przedmiotu.

## Biblioteki CUDA

### cuBLAS

Biblioteka ta implementuje podstawowe operacje algebry linowej. Jest to część CUDA toolkit, więc jest to oprogramowanie wspierane przez korporację NVIDIA.

[https://docs.nvidia.com/cuda/cublas/index.html](https://docs.nvidia.com/cuda/cublas/index.html)

Może obsługiwać zarówno tablice zaalokowane po stronie karty graficznej jak i po stronie hosta (wtedy wykonywane jest nie jawne kopiowanie).

### Liczby losowe

Podstawową biblioteką do generowania liczb losowych jest cuRAND. Jest to część CUDA toolkit, więc jest to oprogramowanie wspierane przez korporację NVIDIA.

Funkcję cuRAND mogą być wywoływane zarówno z poziomu hosta (HOST API) jak i wewnątrz kerneli (DEVICE API).

[https://docs.nvidia.com/cuda/curand/device-api-overview.html#thrust-and-curand-example](https://docs.nvidia.com/cuda/curand/device-api-overview.html#thrust-and-curand-example)


## Operacja redukcji tablicy

### 'Ręczna' implementacja

Bardzo dobry wstęp do różnych aspektów projektowania algorytmów w technologii CUDA dostarcza prezentacja M. Harrisa:
[https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf](https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf)

Należy zaimplementować kolejne wersje kerneli wykonujących redukcję (można kierować się prezentacją w linku powyżej) i porównać czas wykonania kernela redukcji dla jednej tablicy. Tablica powinna być największą tablicą jaką uda się zaalokować na karcie graficznej. Powtórzyć 

### Thrust

Najłatwiej powiedzieć, że Thrust to odpowiednik STL z C++ dla algorytmów wykonywanych na kartach graficznych. Jest to część CUDA toolkit, więc jest to oprogramowanie wspierane przez korporację NVIDIA. Funkcje Thrust są wywoływane z poziomu hosta.

W katalogu `thrust_examples` pokazane są użyteczne przykłady. 

Użyteczne komendy do debugowania:

[https://github.com/thrust/thrust/wiki/Debugging](https://github.com/thrust/thrust/wiki/Debugging)


### CUB

Jest to biblioteka, która nie jest oficjalnie wspierana przez korporację NVIDIA, ale jako jedyna dostarcza łatwe w użyciu szablony powszechnie wykorzystywanych algorytmów, które można używać wewnątrz kerneli. Jest podzielona na operacje działające w ramach jednego warpu/bloku/streamu.

Bibliotekę można sklonować z [https://github.com/NVlabs/cub.git](https://github.com/NVlabs/cub.git) albo pobrać tutaj:
[https://nvlabs.github.io/cub/](https://nvlabs.github.io/cub/) (tu można znaleźć też dokumentację, w której jest dokładnie wyjaśniona jest koncepcja operacji wewnątrz warpów i bloków).

Proponowany katalog instalacji

`~/libs/include`


## Sprawozdanie

Należy opisać wyniki czasów wykonywania redukcji za pomocą algorytmów z poprzedniej sekcji. Należy zaimplementować jak najszybszą wersję algorytmu reduckji i porównać 

Należy przerobić program z laboratorium 2 tak by zamiast redukcji wykonywanych na wielu procesorach (OpenMP) na redukcje wykonywane na kartach graficznych. Można założyć, że jeden proces wykorzystuje jedną kartę graficzną.

Należy sprawdzić czas wykonania dla różnej długości tablicy, ilości procesów MPI oraz różnej ilości wątków w bloku CUDA. Wyniki należy przedstawić w postaci wykresu.

Należy też odpowiedzieć na pytania:

1. Ile czasu trwa kopiowanie danych na kartę graficzną w porównaniu do wygenerowania liczb losowych na karcie graficznej?

2. Czy można 'przykryć' czas kopiowania danych na kartę graficzną i jak to zrobić? 

3. Dla jakiej ilości danych opłaca się używać więcej niż jednej karty graficznej?

Proszę zaproponować co najmniej jedno usprawnienie kodu i je zaimplementować.
