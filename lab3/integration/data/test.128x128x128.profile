==4481== NVPROF is profiling process 4481, command: ./expected_val.exe
==4481== Profiling application: ./expected_val.exe
==4481== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.279088,60.212432,100,0.602124,0.595922,0.617330,"void kernel_RC_mult<int=128, int=128, int=128, int=1>(double const *, double2 const *, double2*)"
20.183489,48.075190,100,0.480751,0.475061,0.495828,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
19.558283,46.586007,100,0.465860,0.461301,0.482485,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.331817,41.282772,100,0.412827,0.405015,0.428534,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.680895,34.968524,100,0.349685,0.347128,0.359320,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.692480,4.031329,3,1.343776,0.000928,2.680801,"[CUDA memcpy HtoD]"
0.372546,0.887370,100,0.008873,0.008512,0.009600,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.240422,0.572662,400,0.001431,0.001343,0.008448,"[CUDA memcpy DtoH]"
0.239332,0.570066,102,0.005588,0.003167,0.245275,"[CUDA memset]"
0.173959,0.414355,100,0.004143,0.004031,0.005088,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.139744,0.332856,100,0.003328,0.003231,0.003520,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.107945,0.257114,100,0.002571,0.002528,0.002656,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4481== API calls:
No API activities were profiled.
