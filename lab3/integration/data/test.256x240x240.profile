==5448== NVPROF is profiling process 5448, command: ./expected_val.exe
==5448== Profiling application: ./expected_val.exe
==5448== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
26.046797,429.363916,100,4.293639,4.234911,4.739476,"void kernel_RC_mult<int=256, int=240, int=240, int=1>(double const *, double2 const *, double2*)"
20.413072,336.495748,100,3.364957,3.334867,3.730859,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.855236,327.300202,100,3.273002,3.242646,3.314452,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.781413,276.630288,100,2.766302,2.738113,2.796672,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.901821,245.646483,100,2.456464,2.412937,2.692994,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.731407,28.541076,3,9.513692,0.001216,18.997454,"[CUDA memcpy HtoD]"
0.117961,1.944500,102,0.019063,0.003168,1.616923,"[CUDA memset]"
0.051391,0.847149,100,0.008471,0.008224,0.009152,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.034400,0.567059,400,0.001417,0.001343,0.001760,"[CUDA memcpy DtoH]"
0.026918,0.443730,100,0.004437,0.004224,0.005056,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.023811,0.392501,100,0.003925,0.003648,0.004384,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.015774,0.260028,100,0.002600,0.002560,0.002720,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5448== API calls:
No API activities were profiled.
