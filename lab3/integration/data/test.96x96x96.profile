==4037== NVPROF is profiling process 4037, command: ./expected_val.exe
==4037== Profiling application: ./expected_val.exe
==4037== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
23.864657,25.184960,100,0.251849,0.248026,0.266202,"void kernel_RC_mult<int=96, int=96, int=96, int=1>(double const *, double2 const *, double2*)"
20.155289,21.270372,100,0.212703,0.208571,0.219835,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
18.824923,19.866404,100,0.198664,0.195291,0.226235,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
18.513725,19.537990,100,0.195379,0.188987,0.203355,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.423422,15.221392,100,0.152213,0.150620,0.162652,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.612203,1.701398,3,0.567132,0.001216,1.136804,"[CUDA memcpy HtoD]"
0.895401,0.944939,100,0.009449,0.009215,0.010016,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.566407,0.597743,400,0.001494,0.001376,0.009536,"[CUDA memcpy DtoH]"
0.419194,0.442386,102,0.004337,0.003167,0.100606,"[CUDA memset]"
0.384053,0.405301,100,0.004053,0.003936,0.004640,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.340725,0.359576,100,0.003595,0.003488,0.003808,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"

==4037== API calls:
No API activities were profiled.
