#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//#include <algorithm>
//#include <chrono>
//#include <random>
#include <set>

#include <cusparse_v2.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

// COMPILE COMMAND:
//nvcc -std=c++11 -o cusparse_gemm.x cusparse_gemm.cu -O3 -gencode=arch=compute_35,code=sm_35 -gencode=arch=compute_60,code=sm_60 -lcusparse -lcudart

#define N 100000

// matrix generation and validation depends on these relationships:
#define SCL 2
#define K N
#define M (SCL*N)
// A: MxK  B: KxN  C: MxN

// error check macros
#define CUSPARSE_CHECK(x)                                               \
    {                                                                   \
        cusparseStatus_t _c=x;                                          \
        if (_c != CUSPARSE_STATUS_SUCCESS) {                            \
            printf("cusparse fail: %d, line: %d\n", (int)_c, __LINE__); \
            exit(-1);}                                                  \
    }

#define CUDA_CHECK(__err) \
    do { \
        if (__err != cudaSuccess) { \
            fprintf(stderr, "Fatal error: %s (at %s:%d)\n", cudaGetErrorString(__err), __FILE__, __LINE__); \
            fprintf(stderr, "*** FAILED - ABORTING\n"); \
            exit(EXIT_FAILURE); \
        } \
    } while (0)


template <typename T>
class SparseMatrix
{
public:
    cusparseMatDescr_t descr;
    cusparseIndexBase_t idxBase;
    
    
    T   *h_val;
    T   *d_val;
    
    // pointers to arrays representing matrix in coordinate format
    int *h_cooRowInd;
    int *h_cooColInd;
    int *d_cooRowInd;
    int *d_cooColInd;
    
    // to convert to csr format we need only one more array
    int *h_csrRowPtr;
    int *d_csrRowPtr;
    int *h_csrColInd;
    int *d_csrColInd;

    int *d_degrees;
    int *h_degrees;
    
    const int nnz; // number of non-zero entries?
    const int n;   // dimension of matrix
    
    // constructors
    //SparseMatrix() {} // default
    /*
    SparseMatrix(int _nnz, int _n):
    nnz(_nnz), n(_n), h_csrRowPtr(NULL), d_csrRowPtr(NULL)
    {
        // allocate memory
        // TODO: probably change to pitched memory
        // TODO: add device?
        h_cooRowInd = (int *) malloc(nnz * sizeof(int));
        h_cooColInd = (int *) malloc(nnz * sizeof(int));
        CUDA_CHECK( cudaMalloc(&d_cooRowInd, nnz * sizeof(int)) );
        CUDA_CHECK( cudaMalloc(&d_cooColInd, nnz * sizeof(int)) );
        
        h_val = (T*) malloc( nnz * sizeof(T) );
        CUDA_CHECK( cudaMalloc(&d_val, nnz*sizeof(T)) );
        
        // create matrix descriptor
        CUSPARSE_CHECK( cusparseCreateMatDescr (&descr)                               );
        CUSPARSE_CHECK( cusparseSetMatType     ( descr, CUSPARSE_MATRIX_TYPE_GENERAL) );
        CUSPARSE_CHECK( cusparseSetMatIndexBase( descr, CUSPARSE_INDEX_BASE_ZERO)     );
        idxBase = CUSPARSE_INDEX_BASE_ZERO;
    }*/
    
    SparseMatrix(int _nnz, int _n, int* _csrRowPtr=NULL):
    nnz(_nnz), n(_n), h_csrRowPtr(NULL), d_csrRowPtr(_csrRowPtr)
    {
        // allocate memory
        // TODO: probably change to pitched memory
        // TODO: add device?
        h_cooRowInd = (int *) malloc(nnz * sizeof(int));
        h_cooColInd = (int *) malloc(nnz * sizeof(int));
        CUDA_CHECK( cudaMalloc(&d_cooRowInd, nnz * sizeof(int)) );
        CUDA_CHECK( cudaMalloc(&d_cooColInd, nnz * sizeof(int)) );
        
        h_val = (T*) malloc( nnz * sizeof(T) );
        CUDA_CHECK( cudaMalloc(&d_val, nnz*sizeof(T)) );
        
        // create matrix descriptor
        CUSPARSE_CHECK( cusparseCreateMatDescr (&descr)                               );
        CUSPARSE_CHECK( cusparseSetMatType     ( descr, CUSPARSE_MATRIX_TYPE_GENERAL) );
        CUSPARSE_CHECK( cusparseSetMatIndexBase( descr, CUSPARSE_INDEX_BASE_ZERO)     );
        idxBase = CUSPARSE_INDEX_BASE_ZERO;
        
        // copy column pointers
        // NOTE: These pointers do not 'own' arrays, but are convenient
        h_csrColInd = h_cooColInd;
        d_csrColInd = d_cooColInd;
    }
    
    
    // destructor
    ~SparseMatrix()
    {
        // free memory
        if (h_cooRowInd) free(h_cooRowInd);
        if (h_cooColInd) free(h_cooColInd);
        if (h_csrRowPtr) free(h_csrRowPtr);
        if (h_val) free(h_val);
        if (d_cooRowInd) CUDA_CHECK( cudaFree(d_cooRowInd) );
        if (d_cooColInd) CUDA_CHECK( cudaFree(d_cooColInd) );
        if (d_csrRowPtr) CUDA_CHECK( cudaFree(d_csrRowPtr) );
        if (d_val)    CUDA_CHECK( cudaFree(d_val) );
        
        // remove matrix descriptor
        if (descr) cusparseDestroyMatDescr(descr);
    }
    
    void to_device()
    {
        if (d_cooRowInd) CUDA_CHECK( cudaMemcpy(d_cooRowInd, h_cooRowInd, nnz*sizeof(int), cudaMemcpyHostToDevice) );
        if (d_cooColInd) CUDA_CHECK( cudaMemcpy(d_cooColInd, h_cooColInd, nnz*sizeof(int), cudaMemcpyHostToDevice) );
        if (d_csrRowPtr) CUDA_CHECK( cudaMemcpy(d_csrRowPtr, h_csrRowPtr, (n+1)*sizeof(int), cudaMemcpyHostToDevice) );
        CUDA_CHECK( cudaMemcpy(d_val,    h_val,     nnz   *sizeof( T ), cudaMemcpyHostToDevice) );
    }
    
    void to_host()
    {
        CUDA_CHECK( cudaMemcpy(h_cooRowInd, d_cooRowInd, nnz*sizeof(int), cudaMemcpyDeviceToHost) );
        CUDA_CHECK( cudaMemcpy(h_cooColInd, d_cooColInd, nnz*sizeof(int), cudaMemcpyDeviceToHost) );
        if (d_csrRowPtr) CUDA_CHECK( cudaMemcpy(h_csrRowPtr, d_csrRowPtr, (n+1)*sizeof(int), cudaMemcpyDeviceToHost) );
        CUDA_CHECK( cudaMemcpy(h_val,    d_val,     nnz   *sizeof( T ), cudaMemcpyDeviceToHost) );
    }
    
    void generate_diagonal()
    {
        for (int i=0; i<n; i++)
        {
            // row and column index
            int irow = i;
            int icol = i;
            
            // row-major indexing 
            h_cooRowInd[i] = irow;
            h_cooColInd[i] = icol;
            h_val[i] = 1;
        }
    }
    
    void generate_random_sym()
    {
        // using coo format to stay sane
        // set of row-major indexes in square matrix
        std::set<int> index;
        while (index.size() < nnz)
        {
            int idx;
            int irow = 0;
            int icol = 0;
            
            // repeat until irow != icol -> we design for zeros on diagonal
            while (irow == icol)
            {
                idx  = (int) (  n*n * ((double) rand() / (RAND_MAX + 1.0)) );
                
                // NOTE: idx = irow*n + icol
                irow = idx/n; icol = idx - irow*n;
            }
            index.insert( idx );
            index.insert( icol*n + irow );
        }
        
        // NOTE: if use c++ iterator for set, the numbers are already sorted
        
        // #pragma omp parallel for
        //for (int i = 0; i < nnz; i++)
        int i = 0; std::set<int>::iterator itr;
        for (itr = index.begin(); itr != index.end(); ++itr)
        {
            // row and column index
            int irow = *itr/n;
            int icol = *itr - irow*n;
            
            // row-major indexing 
            h_cooRowInd[i] = irow;
            h_cooColInd[i] = icol;
            h_val[i] = 1;
            i++;
        }
    }
    
    void print_dense(FILE * stream = stdout)
    {
        int idx = 0;
        if (n < 20) 
        {
            for (int i=0; i<n; i++)
            {
                for (int j=0; j<n; j++)
                {
                    int k = h_cooRowInd[idx]*n + h_cooColInd[idx];
                    if (k == i*n+j)
                    {
                        fprintf(stream,"%1d ",(int) h_val[i]);
                        idx++;
                    }
                    else
                    {
                        fprintf(stream,"0 ");
                    }
                }
                fprintf(stream,"\n");
            }
            fprintf(stream,"# ");
            if (h_csrRowPtr)
            for (int i=0; i<n+1; i++)
            {
                fprintf(stream,"%1d ",(int) h_csrRowPtr[i]);
            }
            fprintf(stream,"\n");
            fprintf(stream,"# ");
            if (h_csrRowPtr)
            for (int i=0; i<nnz; i++)
            {
                fprintf(stream,"%1d ",(int) h_csrColInd[i]);
            }
            fprintf(stream,"\n");
        }
        else
            fprintf(stderr,"# Warinng!  Too large matrix to print in terminal!\n");
    }
    
    void countCsrDegrees(int *d_csr, int *d_degrees, int n) {
        for (int i=0; i<n; i++) {
            printf("%d\n", d_csr[i]);
        }
    }

    void to_csr(cusparseHandle_t* handle)
    {
        
        if (!h_csrRowPtr) h_csrRowPtr = (int *) malloc( (n+1) * sizeof(int) );
        if (!d_csrRowPtr) CUDA_CHECK( cudaMalloc(&d_csrRowPtr, (n+1)*sizeof(int)) );
        if (!h_degrees) h_degrees = (int *) malloc( n * sizeof(int) );
        if (!d_degrees) CUDA_CHECK( cudaMalloc(&d_degrees, n*sizeof(int)) );

        CUSPARSE_CHECK( cusparseXcoo2csr(*handle, d_cooRowInd, nnz, n, d_csrRowPtr, idxBase) );
        
        // NOTE: We don't need to reorder data to have csr format
        // NOTE: Still row-major indexing
        
        // soritng?
        size_t pBufferSizeInBytes = 0;
        void *pBuffer = NULL;
        int *P = NULL;

        // step 1: allocate buffer
        cusparseXcsrsort_bufferSizeExt(*handle, n, n, nnz, d_csrRowPtr, d_csrColInd, &pBufferSizeInBytes);
        cudaMalloc( &pBuffer, sizeof(char)* pBufferSizeInBytes);

        // step 2: setup permutation vector P to identity
        cudaMalloc( (void**)&P, sizeof(int)*nnz);
        cusparseCreateIdentityPermutation(*handle, nnz, P);

        // step 3: sort CSR format
        cusparseXcsrsort(*handle, n, n, nnz, descr, d_csrRowPtr, d_csrColInd, P, pBuffer);
	
	    countCsrDegrees(d_csrRowPtr, d_degrees, n);
	
        // step 4: gather sorted csrVal
        cusparseSgthr(*handle, nnz, d_val, d_val, P, idxBase);
        
        cudaFree(pBuffer);
        cudaFree(P);
    }
    
    
    void to_coo(cusparseHandle_t *handle)
    {
        if (!d_csrRowPtr) fprintf(stderr,"# Warning! d_csrRowPtr is not allocated! No result is returned\n");
//         fprintf(stderr,"# Warning! Not implemented!\n");
        else
        {
            CUSPARSE_CHECK( cusparseXcsr2coo(*handle, d_csrRowPtr, nnz, n, d_cooRowInd, idxBase) );
            if (!h_csrRowPtr) h_csrRowPtr = (int *) malloc( (n+1) * sizeof(int) );
        }
    }
    
    
    
    void generate_simple_csr(const int scl)
    {
        
        if (!h_csrRowPtr) h_csrRowPtr = (int *) malloc( (n+1) * sizeof(int) );
        if (!d_csrRowPtr) CUDA_CHECK( cudaMalloc(&d_csrRowPtr, (nnz+1)*sizeof(int)) );
        
        for (int i = 0; i < n; i++)
        {
            h_val[i] = 1.0f;
            h_csrRowPtr[i] = i;
            h_csrColInd[i] = i/scl;
        }
        h_csrRowPtr[n] = n;
    }
    
};


/*
 * Function multiplying two square matrices of size n.
 */
template <typename T>
SparseMatrix<T>* gemm(cusparseHandle_t hndl, SparseMatrix<T>* A, SparseMatrix<T>* B)
{
    cusparseMatDescr_t descr;
    CUSPARSE_CHECK( cusparseCreateMatDescr (&descr)                               );
    CUSPARSE_CHECK( cusparseSetMatType     ( descr, CUSPARSE_MATRIX_TYPE_GENERAL) );
    CUSPARSE_CHECK( cusparseSetMatIndexBase( descr, CUSPARSE_INDEX_BASE_ZERO)     );
    
    cusparseOperation_t transA = CUSPARSE_OPERATION_NON_TRANSPOSE;
    //cusparseOperation_t transA = CUSPARSE_OPERATION_TRANSPOSE;
    cusparseOperation_t transB = CUSPARSE_OPERATION_NON_TRANSPOSE;
    //cusparseOperation_t transB = CUSPARSE_OPERATION_TRANSPOSE;
    
    // first one have to know number of non-zero entries in resulting matrix
    const int n = A->n;
    int nnz_new;
    int base;
    int* nnz_ptr = NULL;
    int* d_csrRowPtr = NULL;
    CUDA_CHECK( cudaMalloc(&d_csrRowPtr, (n+1) * sizeof(int)) );
    
    CUSPARSE_CHECK( cusparseXcsrgemmNnz(hndl, transA, transB,
                                        n, n, n,
                                        B->descr, B->nnz, B->d_csrRowPtr, B->d_csrColInd,
                                        A->descr, A->nnz, A->d_csrRowPtr, A->d_csrColInd,
                                        descr, d_csrRowPtr, nnz_ptr    ) );
    // NOTE: We don't want to change properties of C, so we can use A.descr instead of not allocated C.descr (tricky)
    // NOTE: If something is wrong with any of arrays d_csrRowPtr then cudaMemcpy will rise illegal memory access
    
    // get nnz
    if (NULL != nnz_ptr) nnz_new = *nnz_ptr;
    else {
        CUDA_CHECK( cudaMemcpy(&base,    d_csrRowPtr,   sizeof(int), cudaMemcpyDeviceToHost) );
        CUDA_CHECK( cudaMemcpy(&nnz_new, d_csrRowPtr+n, sizeof(int), cudaMemcpyDeviceToHost) );
        nnz_new -= base;
    }
    
    // allocate new sparse matrix
    printf("# creating new sparse matrix with %d non-zero entries\n",nnz_new);
    SparseMatrix<T>* C = new SparseMatrix<T>(nnz_new,n,d_csrRowPtr);
    
    // perform multiplication
    // TODO: enable other types than float
    CUSPARSE_CHECK(cusparseScsrgemm(hndl, transA, transB, n, n, n, 
                                    B->descr, B->nnz, B->d_val, B->d_csrRowPtr, B->d_csrColInd,
                                    A->descr, A->nnz, A->d_val, A->d_csrRowPtr, A->d_csrColInd,
                                    C->descr, C->d_val, C->d_csrRowPtr, C->d_csrColInd) );
    /*
    cusparseScsrgemm(cusparseHandle_t handle,
                 cusparseOperation_t transA, 
                 cusparseOperation_t transB,
                 int m, 
                 int n, 
                 int k,
                 const cusparseMatDescr_t descrA, 
                 const int nnzA,
                 const float *csrValA,
                 const int *csrRowPtrA, 
                 const int *csrColIndA,
                 const cusparseMatDescr_t descrB, 
                 const int nnzB,                                     
                 const float *csrValB, 
                 const int *csrRowPtrB, 
                 const int *csrColIndB,
                 const cusparseMatDescr_t descrC,
                 float *csrValC,
                 const int *csrRowPtrC, 
                 int *csrColIndC );
    */
    
    return C;
}



//compile: nvcc -std=c++11 -o cusparse_gemm.x cusparse_gemm.cu -O3 -arch compute_52 -lcusparse -lcudart
int main(int argc, char* argv[])
{
    int n   = 10;
    int nnz = 10;
    SparseMatrix<float> A = SparseMatrix<float>(nnz,n);
    SparseMatrix<float> B = SparseMatrix<float>(nnz,n);
    
    A.generate_random_sym();
    A.to_device();
    A.print_dense();
    printf("\n");
    
    /* */
    B.generate_random_sym();
    //B.generate_diagonal();
    //B.generate_simple_csr(2);
    
    B.to_device();
    B.print_dense();
    printf("\n");
    
    
    
    
    // initialize cusparse
    cusparseHandle_t hndl;
    CUSPARSE_CHECK( cusparseCreate(&hndl) );
    CUSPARSE_CHECK( cusparseSetPointerMode(hndl, CUSPARSE_POINTER_MODE_HOST) );
    // NOTE: This mean that pointers to arrays are stored on host side (not the data in arrays)
    
    
    // convert to csr format
    A.to_csr(&hndl);
    A.to_host();
    FILE* fp = fopen("graphA.txt","w");
    A.print_dense(fp);
    fclose(fp);
    
    
    B.to_csr(&hndl);
    B.to_host();
    FILE* fp1 = fopen("graphB.txt","w");
    A.print_dense(fp1);
    fclose(fp1);
    
    
    // compute third power of matrix A
    // TODO: How to compute n-th power?
//     SparseMatrix<float>* B = gemm<float>(hndl,&A,&A);
    SparseMatrix<float>* C = gemm<float>(hndl,&A,&A);
    
    // convert to coo
    //B->to_coo(&hndl);
    //B->to_host();
    C->to_coo(&hndl);
    C->to_host();
    
    
    // compute trace
    // TODO: move to gpu (and compare results)
    double trace = 0;
    for (int i=0; i < C->nnz; i++)
    {
        if ( C->h_cooRowInd[i] == C->h_cooColInd[i] )
        {
            trace += C->h_val[i];
        }
    }
    
    
    // number of triangles
    //int triangles = (int) (trace/6.0);
    //printf("number of triangles: %d\n",triangles);
    //printf("teoretical estimate: \n");
    
    
    FILE* fp2 = fopen("graphC.txt","w");
    C->print_dense(fp2);
    C->print_dense();
    fclose(fp2);
    
    // cleanup memory before exit
//     delete B;
    delete C;
    
    // here 
    
    
    
    return EXIT_SUCCESS;
}


/* A:
   |1.0 0.0 0.0 ...|
   |1.0 0.0 0.0 ...|
   |0.0 1.0 0.0 ...|
   |0.0 1.0 0.0 ...|
   |0.0 0.0 1.0 ...|
   |0.0 0.0 1.0 ...|
   ...

   B:
   |2.0 0.0 0.0 ...|
   |0.0 2.0 0.0 ...|
   |0.0 0.0 2.0 ...|
   ...                */

/* perform sparse-matrix multiplication C=AxB
 * compile: nvcc -o cusparse_gemm.x cusparse_gemm.cu -O3 -arch compute_53 -lcusparse
int main(){

  cusparseStatus_t stat;
  cusparseHandle_t hndl;
  cusparseMatDescr_t descrA, descrB, descrC;
  int *csrRowPtrA, *csrRowPtrB, *csrRowPtrC, *csrColIndA, *csrColIndB, *csrColIndC;
  int *h_csrRowPtrA, *h_csrRowPtrB, *h_csrRowPtrC, *h_csrColIndA, *h_csrColIndB, *h_csrColIndC;
  float *csrValA, *csrValB, *csrValC, *h_csrValA, *h_csrValB, *h_csrValC;
  int nnzA, nnzB, nnzC;
  int m,n,k;
  m = M;
  n = N;
  k = K;

// generate A, B=2I


  nnzA = m;
  nnzB = n;
  h_csrRowPtrA = (int *)malloc((m+1)*sizeof(int));
  h_csrRowPtrB = (int *)malloc((n+1)*sizeof(int));
  h_csrColIndA = (int *)malloc(m*sizeof(int));
  h_csrColIndB = (int *)malloc(n*sizeof(int));
  h_csrValA  = (float *)malloc(m*sizeof(float));
  h_csrValB  = (float *)malloc(n*sizeof(float));
  if ((h_csrRowPtrA == NULL) || (h_csrRowPtrB == NULL) || (h_csrColIndA == NULL) || (h_csrColIndB == NULL) || (h_csrValA == NULL) || (h_csrValB == NULL))
    {printf("malloc fail\n"); return -1;}
  for (int i = 0; i < m; i++)
  {
    h_csrValA[i] = 1.0f;
    h_csrRowPtrA[i] = i;
    h_csrColIndA[i] = i/SCL;
    if (i < n){
      h_csrValB[i] = 2.0f;
      h_csrRowPtrB[i] = i;
      h_csrColIndB[i] = i;}
    }
  h_csrRowPtrA[m] = m;
  h_csrRowPtrB[n] = n;

// transfer data to device

  cudaMalloc(&csrRowPtrA, (m+1)*sizeof(int));
  cudaMalloc(&csrRowPtrB, (n+1)*sizeof(int));
  cudaMalloc(&csrColIndA, m*sizeof(int));
  cudaMalloc(&csrColIndB, n*sizeof(int));
  cudaMalloc(&csrValA, m*sizeof(float));
  cudaMalloc(&csrValB, n*sizeof(float));
  CUDA_CHECK("cudaMalloc fail");
  cudaMemcpy(csrRowPtrA, h_csrRowPtrA, (m+1)*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(csrRowPtrB, h_csrRowPtrB, (n+1)*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(csrColIndA, h_csrColIndA, m*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(csrColIndB, h_csrColIndB, n*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(csrValA, h_csrValA, m*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(csrValB, h_csrValB, n*sizeof(float), cudaMemcpyHostToDevice);
  CUDA_CHECK("cudaMemcpy fail");

// set cusparse matrix types
  CUSPARSE_CHECK(cusparseCreate(&hndl));
  stat = cusparseCreateMatDescr(&descrA);
  CUSPARSE_CHECK(stat);
  stat = cusparseCreateMatDescr(&descrB);
  CUSPARSE_CHECK(stat);
  stat = cusparseCreateMatDescr(&descrC);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatType(descrB, CUSPARSE_MATRIX_TYPE_GENERAL);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatType(descrC, CUSPARSE_MATRIX_TYPE_GENERAL);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatIndexBase(descrA, CUSPARSE_INDEX_BASE_ZERO);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatIndexBase(descrB, CUSPARSE_INDEX_BASE_ZERO);
  CUSPARSE_CHECK(stat);
  stat = cusparseSetMatIndexBase(descrC, CUSPARSE_INDEX_BASE_ZERO);
  CUSPARSE_CHECK(stat);
  cusparseOperation_t transA = CUSPARSE_OPERATION_NON_TRANSPOSE;
  cusparseOperation_t transB = CUSPARSE_OPERATION_NON_TRANSPOSE;

// figure out size of C
  int baseC;
// nnzTotalDevHostPtr points to host memory
  int *nnzTotalDevHostPtr = &nnzC;
  stat = cusparseSetPointerMode(hndl, CUSPARSE_POINTER_MODE_HOST);
  CUSPARSE_CHECK(stat);
  cudaMalloc((void**)&csrRowPtrC, sizeof(int)*(m+1));
  CUDA_CHECK("cudaMalloc fail");
  stat = cusparseXcsrgemmNnz(hndl, transA, transB, m, n, k,
        descrA, nnzA, csrRowPtrA, csrColIndA,
        descrB, nnzB, csrRowPtrB, csrColIndB,
        descrC, csrRowPtrC, nnzTotalDevHostPtr );
  CUSPARSE_CHECK(stat);
  if (NULL != nnzTotalDevHostPtr){
    nnzC = *nnzTotalDevHostPtr;}
  else{
    cudaMemcpy(&nnzC, csrRowPtrC+m, sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(&baseC, csrRowPtrC, sizeof(int), cudaMemcpyDeviceToHost);
    CUDA_CHECK("cudaMemcpy fail");
    nnzC -= baseC;}
  cudaMalloc((void**)&csrColIndC, sizeof(int)*nnzC);
  cudaMalloc((void**)&csrValC, sizeof(float)*nnzC);
  CUDA_CHECK("cudaMalloc fail");
// perform multiplication C = A*B
  stat = cusparseScsrgemm(hndl, transA, transB, m, n, k,
        descrA, nnzA,
        csrValA, csrRowPtrA, csrColIndA,
        descrB, nnzB,
        csrValB, csrRowPtrB, csrColIndB,
        descrC,
        csrValC, csrRowPtrC, csrColIndC);
  CUSPARSE_CHECK(stat);

// copy result (C) back to host
  h_csrRowPtrC = (int *)malloc((m+1)*sizeof(int));
  h_csrColIndC = (int *)malloc(nnzC *sizeof(int));
  h_csrValC  = (float *)malloc(nnzC *sizeof(float));
  if ((h_csrRowPtrC == NULL) || (h_csrColIndC == NULL) || (h_csrValC == NULL))
    {printf("malloc fail\n"); return -1;}
  cudaMemcpy(h_csrRowPtrC, csrRowPtrC, (m+1)*sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_csrColIndC, csrColIndC,  nnzC*sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_csrValC, csrValC, nnzC*sizeof(float), cudaMemcpyDeviceToHost);
  CUDA_CHECK("cudaMemcpy fail");

// check result, C = 2A
  if (nnzC != m) {printf("invalid matrix size C: %d, should be: %d\n", nnzC, m); return -1;}
  for (int i = 0; i < m; i++){
    if (h_csrRowPtrA[i] != h_csrRowPtrC[i]) {printf("A/C row ptr mismatch at %d, A: %d, C: %d\n", i, h_csrRowPtrA[i], h_csrRowPtrC[i]); return -1;}
    if (h_csrColIndA[i] != h_csrColIndC[i]) {printf("A/C col ind mismatch at %d, A: %d, C: %d\n", i, h_csrColIndA[i], h_csrColIndC[i]); return -1;}
    if ((h_csrValA[i]*2.0f) != h_csrValC[i]) {printf("A/C value mismatch at %d, A: %f, C: %f\n", i, h_csrValA[i]*2.0f, h_csrValC[i]);   return -1;}
    }
  printf("Success!\n");
  return 0;
}
*/
